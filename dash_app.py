import dash
from dash.dependencies import State, Input, Output
import dash_core_components as dcc
import dash_html_components as html

from app import bingads_blueprint, google


app = dash.Dash('dash_app')

external_css = ["https://fonts.googleapis.com/css?family=Product+Sans:400,400i,700,700i",
                "https://cdn.rawgit.com/plotly/dash-app-stylesheets/2cc54b8c03f4126569a3440aae611bbef1d7a5dd/stylesheet.css"]

for css in external_css:
    app.css.append_css({"external_url": css})

app.layout = html.Div([
    html.Div([

        html.H2('Google and Bing Authentication',
                style={'display': 'inline',
                       'float': 'left',
                       'font-size': '2.65em',
                       'margin-left': '7px',
                       'font-weight': 'bolder',
                       'font-family': 'Product Sans',
                       'color': "rgba(117, 117, 117, 0.95)",
                       'margin-top': '20px',
                       'margin-bottom': '0'
                       }),
        html.Img(src="https://s3-us-west-1.amazonaws.com/plotly-tutorials/logo/new-branding/dash-logo-by-plotly-stripe.png",
                style={
                    'height': '100px',
                    # 'float': 'right'
                },
        ),
    ]),
    html.Br(),

    html.Button(html.A("Login to BingAds", href="/login/bingads")),
    html.Br(),
    html.Button(html.A("Login to Google AdWords", href="/login/google")),

    dcc.Dropdown(
        id='picker',
        placeholder="Select OAuth provider to display credentials",
        options=[{'label': 'Google AdWords', 'value':'google'},
                 {'label': 'Bing Ads', 'value':'bing'}],
        value=[],
        multi=False
    ),

    html.Div(id="oauthdataplaceholder")

], className="container")


@app.callback(Output('oauthdataplaceholder', 'children'),
             [Input('picker', 'value')])
def print_oauth(value):
    if value == "google":
        return repr(google.token)
    elif value == "bing":
        return repr(bingads_blueprint.session.token)
