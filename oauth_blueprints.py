from flask_dance.contrib.google import make_google_blueprint, google
from flask_dance.consumer import OAuth2ConsumerBlueprint

import config

google_blueprint = make_google_blueprint(
    client_id=config.GOOGLE_CLIENT_ID,
    client_secret=config.GOOGLE_CLIENT_SECRET,
    scope=[
        "https://www.googleapis.com/auth/adwords",
        "openid",
        "https://www.googleapis.com/auth/userinfo.email",
    ]
)

bingads_blueprint = OAuth2ConsumerBlueprint(
    "bingads", __name__,
    client_id=config.BING_CLIENT_ID,
    client_secret=config.BING_CLIENT_SECRET,
    scope="bingads.manage",
    base_url="https://bingads.microsoft.com",
    token_url="https://login.live.com/oauth20_token.srf",
    authorization_url="https://login.live.com/oauth20_authorize.srf",
)
