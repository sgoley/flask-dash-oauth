from oauth_blueprints import google_blueprint, bingads_blueprint, google
from dash_app import app
import config

server = app.server
server.secret_key = "supersekrit"
server.register_blueprint(google_blueprint, url_prefix="/login")
server.register_blueprint(bingads_blueprint, url_prefix="/login")

if __name__ == "__main__":
    app.run_server()
